package com.devfun.cartoon.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.devfun.cartoon.R;
import com.devfun.cartoon.adapter.ChannelAdapter;
import com.devfun.cartoon.helper.ItemDecorationAlbumColumns;
import com.devfun.cartoon.helper.OnItemClickListener;
import com.devfun.cartoon.model.BaseModel;
import com.devfun.cartoon.model.ChannelModel;
import com.devfun.cartoon.utils.AppUtils;
import com.devfun.cartoon.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadAds();

        RecyclerView fRecyclerView = (RecyclerView) findViewById(R.id.activityMain_recyclerView);
        fRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        fRecyclerView.addItemDecoration(new ItemDecorationAlbumColumns(2, 20, true));
        //
        List<ChannelModel> fModels = AppUtils.getInstance()
                .loadJSONFromAsset(getApplicationContext());
        ChannelAdapter fAdapter = new ChannelAdapter();
        fAdapter.setData(fModels);
        fRecyclerView.setAdapter(fAdapter);
        fAdapter.setListener(this);
    }

    @Override
    public void onItemClick(BaseModel item, int position) {
        if (item instanceof ChannelModel) {
            Intent fIntent = new Intent(getApplicationContext(), VideoOfChannelActivity.class);
            fIntent.putExtra("channelId", ((ChannelModel) item).getId());
            fIntent.putExtra("channelName", ((ChannelModel) item).getName());
            startActivity(fIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                Intent fIntent = new Intent(getApplicationContext(), VideoOfChannelActivity.class);
                startActivity(fIntent);
                break;
            case R.id.more_apps:
                Utils.moreApp(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.rate_msg);
        builder.setCancelable(true);

        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);

        builder.setPositiveButton(R.string.rate_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utils.rateUs(MainActivity.this);
            }
        });

        builder.setNeutralButton(R.string.ramind_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                MainActivity.super.onBackPressed();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void loadAds() {
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
    }
}
